package first;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
public class FirstController {

    private final HelloComponent helloComponent;

    @GetMapping("/hello")
    String greeting(){
        return helloComponent.sayHello();
    }
}
