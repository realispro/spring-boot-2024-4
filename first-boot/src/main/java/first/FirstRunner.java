package first;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class FirstRunner implements CommandLineRunner {

    private final HelloComponent helloComponent;

    @Override
    public void run(String... args) throws Exception {
        log.info("let's start. hello text: {}", helloComponent.sayHello());
    }
}
