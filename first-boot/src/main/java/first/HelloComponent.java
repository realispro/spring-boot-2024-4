package first;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Lazy
public class HelloComponent {

    @Value("${first.hello.name:Jack}")
    private String name;

    public HelloComponent(){
        log.info("component construction");
    }

    public String sayHello(){
        return "Hey " + name + "!";
    }
}
