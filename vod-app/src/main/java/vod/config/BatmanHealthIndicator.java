package vod.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;
import vod.service.MovieService;

@Component
@RequiredArgsConstructor
public class BatmanHealthIndicator implements HealthIndicator {

    private final MovieService movieService;
    @Override
    public Health health() {
        if(movieService.getAllMovies().stream()
                .anyMatch(movie->movie.getTitle().equals("Batman"))){
            return Health.down().build();
        } else {
            return Health.up().build();
        }
    }
}
