package vod.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import vod.web.TimeWindowInterceptor;

import javax.sql.DataSource;

@Configuration
@ComponentScan("vod")
@RequiredArgsConstructor
@EnableMethodSecurity
public class VodConfig implements WebMvcConfigurer {

    private final TimeWindowInterceptor timeWindowInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(timeWindowInterceptor);
    }

    @Bean
    JdbcTemplate jdbcTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }

    @Bean
    UserDetailsService userDetailService(){
        UserDetails user1 = User.withUsername("user1")
                .password("user1")
                .authorities("ROLE_ADMIN")
                .build();
        UserDetails user2 = User.withUsername("user2")
                .password("user2")
                .authorities("ROLE_USER")
                .build();

        return new InMemoryUserDetailsManager(user1, user2);
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        http.csrf(csrf->csrf.disable());

        http.authorizeHttpRequests(registry->registry
                //.requestMatchers(HttpMethod.POST, "/movies").hasRole("ADMIN")
                .requestMatchers(HttpMethod.GET, "/cinemasCount").authenticated()
                .anyRequest().permitAll());

        http.httpBasic(Customizer.withDefaults());

        return http.build();
    }
}
