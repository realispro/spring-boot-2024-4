package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.time.LocalTime;

@Component
public class TimeWindowInterceptor implements HandlerInterceptor {

    @Value("${vod.opening:9}")
    private int openingHour;
    @Value("${vod.closing:13}")
    private int closingHour;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        int currentHour = LocalTime.now().getHour();
        if(currentHour < openingHour || currentHour >= closingHour) {
            response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
            response.getWriter().println("System closed");
            return false;
        } else {
            return true;
        }
    }
}
