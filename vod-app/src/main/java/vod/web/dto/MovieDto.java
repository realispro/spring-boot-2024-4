package vod.web.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MovieDto {
    private int id;
    @NotNull
    @Size(min = 2)
    private String title;
    @URL
    private String poster;
    @Min(1)
    private int directorId;
}
