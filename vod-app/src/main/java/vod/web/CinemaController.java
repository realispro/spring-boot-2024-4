package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieReporter;
import vod.service.MovieService;
import vod.web.dto.CinemaDto;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;
    private final MovieReporter movieReporter;

    @GetMapping("/cinemasCount")
    int countMovies() throws ExecutionException, InterruptedException {
        log.info("about to get cinemas count from reporter");
        Future<Integer> futureCount = movieReporter.countCinemas();
        log.info("future count received");
        return futureCount.get();
    }

    @GetMapping("/cinemas")
    List<CinemaDto> getCinemas(@RequestParam(value = "movieId", required = false) Integer movieId,
                               @RequestHeader(value = "Foo", required = false) String fooHeader,
                               @RequestHeader Map<String, String> header){
        log.info("about to retrieve cinemas");
        log.info("movieId request param: {}", movieId);
        log.info("foo header: {}", fooHeader);
        header.entrySet().forEach(entry->log.info("header: {}:{}", entry.getKey(), entry.getValue()));
        List<Cinema> cinemas = cinemaService.getAllCinemas();
        log.info("{} cinemas found", cinemas.size());
        return cinemas.stream()
                .map(this::toDto).toList();
    }

    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<CinemaDto> getCinema(@PathVariable("cinemaId") int cinemaId){
        log.info("about to retrieve cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if(cinema!=null) {
            return ResponseEntity.ok(toDto(cinema));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/movies/{movieId}/cinemas")
    ResponseEntity<List<CinemaDto>> getCinemasShowingMovie(@PathVariable("movieId") int movieId){
        log.info("about to retrieve cinemas showing movie {}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        if(movie!=null) {
            List<Cinema> cinemas = cinemaService.getCinemasByMovie(movie);
            return ResponseEntity.ok(cinemas.stream()
                    .map(this::toDto)
                    .toList());
        } else {
            return ResponseEntity.notFound().build();
        }

    }


    private CinemaDto toDto(Cinema cinema){
        return CinemaDto.builder()
                .id(cinema.getId())
                .name(cinema.getName())
                .logo(cinema.getLogo())
                .movies(cinema.getMovies().stream()
                        .map(Movie::getId)
                        .toList()
                )
                .build();
    }

}
