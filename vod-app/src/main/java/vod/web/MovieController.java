package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;
import vod.web.dto.MovieDto;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
@Adviced
public class MovieController {
    private final CinemaService cinemaService;
    private final MovieService movieService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;


    @GetMapping("/movies")
    ResponseEntity<List<MovieDto>> getMovies(){
        List<Movie> movies = movieService.getAllMovies();
        return ResponseEntity.ok(movies.stream()
                .map(this::toDto).toList());
    }

    @GetMapping("/movies/{movieId}")
    ResponseEntity<MovieDto> getMovie(@PathVariable("movieId") int movieId){
        Movie movie = movieService.getMovieById(movieId);
        if(movie!=null){
            return ResponseEntity.ok(toDto(movie));
        }
        else return ResponseEntity.notFound().build();
    }

    @GetMapping("/cinemas/{cinemaId}/movies")
    ResponseEntity<List<MovieDto>> getCinemasShowingMovie(@PathVariable("cinemaId") int cinemaId){
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if(cinema!=null){
            List<Movie> movies = cinemaService.getMoviesInCinema(cinema);
            return ResponseEntity.ok(movies.stream()
                    .map(this::toDto)
                    .toList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(@Validated @RequestBody MovieDto movieDto, Errors errors, HttpServletRequest request){
        log.info("about to add movie {}", movieDto);
        // TODO validate data
        if(errors.hasErrors()){
            Locale locale = localeResolver.resolveLocale(request);
                    //new Locale("pl", "PL");
            // messages_fr_CA
            // messages_fr
            // messages_<OS_LANG>
            // messages
            List<String> errorMessages = errors.getAllErrors().stream()
                    .map(oe->messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .toList();
            return ResponseEntity.badRequest().body(errorMessages);
        }

        if(movieDto.getTitle().equals("Aquaman")){
            throw new IllegalArgumentException("invalid superhero");
        }

        Movie movie = movieService.addMovie(fromDto(movieDto));

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{movieId}")
                .build(Map.of("movieId", movie.getId()));
        return ResponseEntity.created(uri).body(toDto(movie));
    }


    private MovieDto toDto(Movie movie) {
        return MovieDto.builder()
                .id(movie.getId())
                .title(movie.getTitle())
                .poster(movie.getPoster())
                .directorId(movie.getDirector().getId())
                .build();
    }

    private Movie fromDto(MovieDto movieDto) {
        Movie movie = new Movie();
        movie.setId(movieDto.getId());
        movie.setTitle(movieDto.getTitle());
        movie.setPoster(movieDto.getPoster());
        movie.setDirector(movieService.getDirectorById(movieDto.getDirectorId()));
        return movie;
    }


}

