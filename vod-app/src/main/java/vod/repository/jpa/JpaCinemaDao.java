package vod.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.model.Cinema;
import vod.model.Movie;
import vod.repository.CinemaDao;

import java.util.List;
import java.util.Optional;

@Repository
@ConditionalOnProperty(name = "vod.dao.type", havingValue = "db")
public abstract class JpaCinemaDao implements CinemaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Cinema> findAll() {
        return entityManager.createQuery("select c from Cinema c", Cinema.class).getResultList();
    }

    @Override
    public Optional<Cinema> findById(Integer id) {
        return Optional.ofNullable(entityManager.find(Cinema.class, id));
    }

    @Override
    public List<Cinema> findByMovie(Movie m) {
        return entityManager.createQuery("select c from Cinema c join c.movies movie where movie=:movie")
                .setParameter("movie", m).getResultList();
    }

    @Override
    public Cinema save(Cinema c) {
        entityManager.persist(c);
        return c;
    }
}
