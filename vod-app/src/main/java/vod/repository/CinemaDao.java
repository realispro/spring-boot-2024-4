package vod.repository;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vod.model.Cinema;
import vod.model.Movie;

import java.util.List;
import java.util.Optional;

//@ConditionalOnProperty(name = "vod.dao.type", havingValue = "data")
public interface CinemaDao extends JpaRepository<Cinema, Integer> {

    //List<Cinema> findAll();

    //Optional<Cinema> findById(Integer id);

    @Query("select c from Cinema c inner join c.movies movie where movie=:movie")
    List<Cinema> findByMovie(@Param("movie") Movie m);

    //Cinema save(Cinema c);

}
