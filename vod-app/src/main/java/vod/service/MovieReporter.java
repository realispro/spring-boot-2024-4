package vod.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.*;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Service
@RequiredArgsConstructor
@Slf4j
//@EnableScheduling
@EnableAsync
public class MovieReporter {

    private final MovieService movieService;
    private final CinemaService cinemaService;

    @Scheduled(cron = "*/5 * * * * *")
            //(fixedDelay = 3_000)
            //(fixedRate = 5_000)
    void reportMoviesCount() throws InterruptedException {
        log.info("Movie(s) count: {}", movieService.getAllMovies().size());
        Thread.sleep(5000);
        log.info("reporting finished.");
    }

    @Async
    public Future<Integer> countCinemas(){
        log.info("about to count cinemas");
        CompletableFuture<Integer> future = new CompletableFuture<>();
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        future.complete(cinemaService.getAllCinemas().size());
        return future;
    }
/*
    @Bean
    TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(5);
        taskScheduler.setThreadNamePrefix("vod-scheduler-");
        return taskScheduler;
    }*/

}
