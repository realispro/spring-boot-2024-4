package vod.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import vod.model.Cinema;
import vod.model.Movie;
import vod.model.Rating;

import java.util.List;

@Component
@Slf4j
public class VodRunner implements CommandLineRunner {
    private final CinemaService service;
    private final MovieService movieService;

    public VodRunner(CinemaService service, MovieService movieService) {
        this.service = service;
        this.movieService = movieService;
    }

    @Override
    public void run(String... args) throws Exception {
        List<Cinema> cinemas = service.getAllCinemas();
        log.info(cinemas.size() + " cinemas found:");
        cinemas.forEach(System.out::println);

        Movie movie = movieService.getMovieById(2);
        List<Rating> ratings = movieService.getRatingsByMovie(movie);
        log.info("Rating of movie {}", movie.getTitle());
        ratings.forEach(rating -> log.info("rate: {}", rating.getRate()));
    }
}
