# Docker
build image:
```
docker build -t vod-app:1.0 .
```

run container:
```
docker run --name vod-app -p 8080:8080 vod-app:1.0
```

# Docker Compose
define service:
```
vod-app:
    hostname: vod-app
    image: vod-app
    ports:
        - 8080:8080
```
run service:
```
docker compose -f docker-compose.yml up vod-app
```

# K8s
`kubectl` executable must be available (one provided with Docker).
It requires cluster installation/integration, eg. kind or minikube
## minikube

Download minikube and place in a location pointed in PATH. Rename it to minikube:
https://github.com/kubernetes/minikube/releases/latest/download/minikube-windows-amd64.exe

start minikube cluster:
```
minikube start
```

load docker image to minikube:
```
minikube image load vod-app:1.0
```

create deployment and service definitions, store it in a yml file:
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: vod-app
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      app: vod-app
  template:
    metadata:
      labels:
        app: vod-app
    spec:
      containers:
        - image: vod-app:1.0
          name: vod-app
          imagePullPolicy: Never
          ports:
            - containerPort: 8080

status: { }

---

apiVersion: v1
kind: Service
metadata:
  name: vod-app
  namespace: default
spec:
  ports:
    - port: 8080
      targetPort: 8080
  selector:
    app: vod-app
  type: LoadBalancer
status:
  loadBalancer: { }

---
```

Apply configuration:
```
kubectl apply -f vod-app.yml
```

verify configuration status:
```
kubectl get all
```

start port forwarding:
```
kubectl port-forward service/vod-app 8080:8080
```

... and enjoy application :)


