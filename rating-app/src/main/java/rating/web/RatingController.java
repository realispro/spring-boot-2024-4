package rating.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import rating.model.Rating;
import rating.service.RatingService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RatingController {

    private final RatingService ratingService;

    @GetMapping("/ratings")
    List<Rating> getRatingsOfMovie(@RequestParam int movieId){
        return ratingService.getRatingsByMovieId(movieId);
    }

    @PostMapping("/ratings")
    ResponseEntity<?> createRating(@Validated @RequestBody Rating rating, Errors errors){

        if(errors.hasErrors()){
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }

        rating = ratingService.addRating(rating);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(rating);
    }
}
