package rating.service;


import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import rating.model.Rating;
import rating.repository.RatingRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class RatingService {

    private final RatingRepository ratingRepository;


    public List<Rating> getRatingsByMovieId(int movieId){
        log.info("about to retrieve ratings of a movie {}", movieId);
        return ratingRepository.findAllByMovieId(movieId);
    }

    public Rating addRating(Rating rating){
        log.info("about to persist rating {}", rating);

        if(!checkIfEMovieExists(rating.getMovieId())){
            throw new IllegalArgumentException("invalid movie id " + rating.getMovieId());
        }

        rating = ratingRepository.save(rating);
        log.info("rating persisted.");

        return rating;
    }

    private boolean checkIfEMovieExists(int movieId){

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<MovieDto> responseEntity = restTemplate
                    .exchange(
                            "http://localhost:8080/movies/" + movieId,
                            HttpMethod.GET,
                            HttpEntity.EMPTY,
                            MovieDto.class
                    );

            return responseEntity.getStatusCode().is2xxSuccessful();
        }catch (HttpClientErrorException e){
            log.error("client exception", e);
            return false;
        }
    }

    @Data
    private static class MovieDto {

        private int id;
        private String title;
        private String poster;
        private int directorId;
    }
}
