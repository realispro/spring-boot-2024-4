package rating.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rating.model.Rating;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Integer> {

    List<Rating> findAllByMovieId(int movieId);
}
