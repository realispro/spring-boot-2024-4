package travel;

public interface Transportation {

    void transport(Person p);
}
