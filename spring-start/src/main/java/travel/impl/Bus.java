package travel.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import travel.Person;
import travel.Transportation;

@Component("transportation")
@Lazy
@Scope("prototype")
public class Bus implements Transportation {

    private String name;

    public Bus(String name) {
        this.name = name;
        System.out.println("[BUS] name = " + name);
    }

    @Override
    public void transport(Person p) {
        System.out.println("person '" + p + "' is being transported by bus");
    }

}
