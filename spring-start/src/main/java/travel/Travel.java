package travel;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Travel {

    private final String name;
    private final Transportation transportation;
    private final Accomodation accomodation;

    //@Autowired
    public Travel(@Value("#{'['+'${travel.name}'.toUpperCase()+']'}") String name, @Qualifier("fast") Transportation transportation, Accomodation accomodation) {
        this.transportation = transportation;
        this.accomodation = accomodation;
        this.name = name;
        System.out.println("constructing travel object using parametrized constructor...");
    }

    public void travel(Person p){
        System.out.println("started travel '" + name + "' for a person " + p);
        transportation.transport(p);
        accomodation.host(p);
        transportation.transport(p);
    }

    @Override
    public String toString() {
        return "Travel{" +
                "name='" + name + '\'' +
                ", transportation=" + transportation +
                ", accomodation=" + accomodation +
                '}';
    }
}
