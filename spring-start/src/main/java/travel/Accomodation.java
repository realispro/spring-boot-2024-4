package travel;

public interface Accomodation {

    void host(Person p);
}
