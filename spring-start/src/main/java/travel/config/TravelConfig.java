package travel.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

@Configuration
@ComponentScan("travel")
@PropertySource("classpath:travel.properties")
public class TravelConfig {

    @Bean("travelName")
    String travelName(){
        return "Winter holiday 2025";
    }

    @Bean
    List<String> meals(){
        return List.of(
                "pomidorowa",
                "schabowy",
                "sernik"
        );
    }
}
