package travel;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class TravelRunner implements CommandLineRunner {

    private final Travel travel;

    public TravelRunner(Travel travel) {
        this.travel = travel;
    }

    @Override
    public void run(String... args) throws Exception {
        Person kowalski = new Person("Jan", "Kowalski", new Ticket(LocalDate.now()));
        travel.travel(kowalski);

    }
}
