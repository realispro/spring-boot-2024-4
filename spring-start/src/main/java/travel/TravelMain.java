package travel;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import travel.config.TravelConfig;
import travel.impl.Bus;
import travel.impl.Hotel;

import java.time.LocalDate;

public class TravelMain {

    public static void main(String[] args) {

        Person kowalski = new Person("Jan", "Kowalski", new Ticket(LocalDate.now()));

        // service preparation
        ApplicationContext context = //new ClassPathXmlApplicationContext("context.xml.bak");
                new AnnotationConfigApplicationContext(TravelConfig.class);

        Travel travel = context.getBean(Travel.class);

        // service use
        travel.travel(kowalski);

        System.out.println("done.");
    }
}
